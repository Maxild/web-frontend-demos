# README #

This README would normally document whatever steps are necessary to get your application up and running.

### Indeholder forskellige demoer for Front-end web development ###

* Susy + Bootstrap eksempel
* Angular + Browserify eksempel (next)
* 

### Liste af teknologier ###

Tools bliver udelukkende benyttet til at bygge applikationer. 

Libraries bliver benyttet i browseren.

#### Tools ####

* Gulp
* Sass + Compass + Bootstrap-sass-components (requires Ruby. Bourbon can be used with LibSass!)
* JSHint
* BrowserSync
* Browserify
* Karma (Testem)
* Protractor
* Jasmine (Mocha, Chai)

#### Libraries ####

* Angular
* susy2, breakpoint-sass (Bootstrap-sass)
* Fontawesome

### Mardown referencer ###

* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)