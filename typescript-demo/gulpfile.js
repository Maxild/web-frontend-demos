'use strict';

/*exported debug*/
var debug = require('gulp-debug');

var gulp = require('gulp');
var sourcemaps = require('gulp-sourcemaps');
var concat = require('gulp-concat');
var del = require('del');
var tsc = require('gulp-typescript');
var mocha = require('gulp-mocha');

gulp.task('clean', function() {
  del(['build/**/*'], function (err, deletedFiles) {
    console.log('Files deleted:', deletedFiles.join(', '));
  });
});

// A wrapper around gulp-rename to support `dirnamePrefix`.
function rename(obj) {
    return rename_(function (parsedPath) {
        return {
            extname: obj.extname || parsedPath.extname,
            dirname: (obj.dirnamePrefix || '') + parsedPath.dirname,
            basename: parsedPath.basename
        };
    });
}

gulp.task('build:src', function () {
    return gulp.src(['src/**/*.ts', 'typings/**/*.d.ts'])
      .pipe(sourcemaps.init())
          .pipe(tsc({
              target: 'ES5',
              declarationFiles: false, // no *.d.ts defini    tion files should be generated
              noExternalResolve: true  // requires and references are not resolved (=> perf)
          }))
          .pipe(concat('app.js'))
      .pipe(sourcemaps.write()) // Now the sourcemaps are added to the .js file
      .pipe(gulp.dest('build/js'));
});

gulp.task('build:test', function () {
    return gulp.src(['test/**/*.ts', 'typings/**/*.d.ts'])
      .pipe(sourcemaps.init())
          .pipe(tsc({
              module: 'commonjs',
              target: 'ES5',
              declarationFiles: false, // no *.d.ts defini    tion files should be generated
              noExternalResolve: true  // requires and references are not resolved (=> perf)
          }))
          .pipe(concat('specs.js'))
      .pipe(sourcemaps.write()) // Now the sourcemaps are added to the .js file
      .pipe(gulp.dest('test'));
});

gulp.task('build', ['build:src', 'build:test']);

gulp.task('test', ['build:test'], function () {
    return gulp.src('test/**/*.js', { read: false })
        .pipe(mocha({ reporter: 'spec' }));
});

gulp.task('watch', ['scripts'], function () {
    gulp.watch('src/*.ts', ['scripts']);
});
