module Validators {
    export interface StringValidator {
        isAcceptable(s: string): boolean;
    }
}