/// <reference path="../typings/tsd.d.ts" />

import chai = require('chai');

var expect = chai.expect;

describe('feature x', () => {
    it('does foo', (done) => {
        expect('bar').to.equal('bar');
        done();
    });
});

describe('User Model Unit Tests:', () => {

    describe('2 + 4', () => {
        it('should be 6', (done) => {
            expect(2 + 4).to.equals(6);
            done();
        });

        it('should not be 7', (done) => {
            expect(2 + 4).to.not.equals(7);
            done();
        });
    });
});
