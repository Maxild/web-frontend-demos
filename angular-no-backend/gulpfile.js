'use strict';

var gulp = require('gulp');

// expose all plugins from package.json on $
var $ = require('gulp-load-plugins')({
  pattern: ['gulp-*', 'del', 'util', 'browser-sync']
});

function handleError(err) {
  console.error(err.toString());
  this.emit('end');
}

function browserSyncInit(baseDir, files, browser) {
  browser = browser === undefined ? 'default' : browser;

  var routes = null;
  // Are we serving files from the ./src directory
  if(baseDir === 'src' || ($.util.isArray(baseDir) && baseDir.indexOf('src') !== -1)) {
    routes = {
      // Should be '/bower_components': '../bower_components'
      // Waiting for https://github.com/shakyShane/browser-sync/issues/308
      '/bower_components': 'bower_components'
    };
  }

  $.browserSync.instance = $.browserSync.init(files, {
    startPath: '/index.html',
    server: {
      baseDir: baseDir,
      //middleware: middleware,
      routes: routes
    },
    port: 3001,
    browser: browser
  });

}

gulp.task('browser-sync', function(cb) {
    $.browserSync({
        // HTML/JS/CSS files to be watched (=> inject, full-page reload)
        files: ['./css/*.css', './js/*.js', './index.html'],
        // Use the built-in static server for basic HTML/JS/CSS websites.
        server: {
            baseDir: [
              './',
              '.tmp'
            ],
            routes: {
              //  the value of each route is a relative path to your cwd
              '/bower_components': '../bower_components',
              //'/bower_components': './bower_components',
            }
        },
        // localhost:3001
        port: 3001,
        // Open the site in chrome anf firefox
        browser: ['google chrome', 'firefox']
      },
      function (err) {
        if (!err) {
          console.log('BrowserSync is ready!');
        }
        else {
          console.error('Error', err.message);
        }
        cb();
      }
    );
});

gulp.task('clean', function (done) {
  $.del(['.tmp', 'dist'], done);
});

// Create css files using ryby sass compiler (css processor)
gulp.task('styles', function() {
  var reload        = $.browserSync.reload;

  // TODO: Use 'src/{app,components}/**/*.scss' when plugin supports it
  return $.rubySass('src/styles', {
      style: 'expanded',
      compass: true,
      require: ['susy'],
      sourcemap: true
    })
    .on('error', handleError)
    .pipe($.autoprefixer('last 1 version'))
    .pipe($.sourcemaps.write()) // Inline sourcemaps (requires gulp-ruby-sass@1.0.0-alpha)
    .pipe(gulp.dest('.tmp/styles'))
    .pipe(reload({stream:true}));

     // TODO: We shouldn't minify scripts here...build/package does that (dist folder)

      //.pipe(rename({suffix: '.min'}))
      //.pipe(minifycss())
      //.pipe(gulp.dest('css'));

      //.pipe(gulp.dest('.tmp'))
      //.pipe(size());
});

// TODO: Only uglify/minify to dist (production)
// concatenate all source files into app.js
gulp.task('scripts', function () {
  // The module js file must be defined first (only requirement of angulars DI subsystem)
  // That is controllers, services etc. can be defined in any order.
  //
  // Important that we first bring in the *.module.js files then all
  // others after. This is because module declaration has to come
  // before defining any resources on the module e.g. the module needs
  // to exist to be appended to. This can work with the below
  // declaration because we have the convention of always defining
  // modules in a *.module.js file in the root of their directory and we
  // rely on the fact that gulp is smart enough to not include the
  // same file twice.
  gulp.src(['src/app/*.module.js', 'src/{app,components}/**/*.js'])
    .pipe($.jshint())
    .pipe($.jshint.reporter('jshint-stylish'))
    //.pipe($.sourcemaps.init())
      .pipe($.concat('app.all.js'))
      // rebuild all AngularJS dependency injection annotations
      .pipe($.ngAnnotate({
        remove: true,
        add: true,
        single_quotes: true
      }))
      //.pipe($.uglify())
      //.pipe($.sourcemaps.write()) // Inline sourcemaps??
    .pipe(gulp.dest('.tmp/scripts/'))
    //.pipe(gulp.dest('./dist/js/'));
    .pipe($.size());
});

// Create js files that will load all partials using $templateCache service
gulp.task('partials', function () {
  return gulp.src('src/{app,components}/**/*.html')
    // .pipe($.minifyHtml({
    //   empty: true,
    //   spare: true,
    //   quotes: true
    // }))
    .pipe($.ngHtml2js({
      moduleName: 'app'
    }))
    .pipe(gulp.dest('.tmp'))
    .pipe($.size());
});

// inject bower components (css, js) in src/index.html
gulp.task('wire-bower-dep', function () {
  var wiredep = require('wiredep').stream;

  return gulp.src('src/index.html')
    .pipe(wiredep())
    .on('error', handleError)
    .pipe(gulp.dest('src'));
});

gulp.task('watch', ['styles', 'scripts', 'wire-bower-dep'] ,function () {
  gulp.watch('src/{app,components}/**/*.scss', ['styles']);  // compiling sass -> css
  gulp.watch('src/{app,components}/**/*.js', ['scripts']);   // concatenation
  // gulp.watch('src/assets/images/**/*', ['images']);
  gulp.watch('bower.json', ['wire-bower-dep']);
});

// serve static site from src and .tmp, and watch files
gulp.task('serve', ['watch'], function () {
  browserSyncInit(
    ['.tmp','src'], // baseDir
    [
    '.tmp/{app,components}/**/*.css',   // watch all css files
    //'src/assets/images/**/*',
    'src/*.html',                       // watch index.html
    'src/{app,components}/**/*.html',   // watch all partials
    'src/{app,components}/**/*.js'      // watch all js source
  ]);
});

// serve from dist (baseDir), and do not watch files
// gulp.task('serve:dist', ['build'], function () {
//   browserSyncInit('dist');
// });

// serve from src (baseDir), and do not watch files
// gulp.task('serve:e2e', function () {
//   browserSyncInit(['src', '.tmp'], null, []);
// });

// gulp.task('serve:e2e-dist', ['watch'], function () {
//   browserSyncInit('dist', null, []);
// });
