(function () {
  'use strict';

  var app = angular.module('app', [
    'ngRoute' // TODO: use ui-router
  ]);

  function routeConfig($routeProvider) {

    $routeProvider.when('/', {
      templateUrl: 'app/main/main.html',
      controller: 'MainCtrl'
    })
    .otherwise({
      redirectTo: '/'
    });
  }

  app.config(routeConfig);

})();
