(function () {
  'use strict';

  function MainCtrl ($scope) {
     $scope.message = 'Hello Morten';
  }

  angular.module('app')
    .controller('MainCtrl', MainCtrl);

})();
