'use strict';

var gulp          = require('gulp'),
    del           = require('del'),
    rubySass      = require('gulp-ruby-sass'),
    libSass       = require('gulp-sass'),
    size          = require('gulp-size'),
    autoprefixer  = require('gulp-autoprefixer'),
    minifycss     = require('gulp-minify-css'),
    rename        = require('gulp-rename'),
    sourcemaps    = require('gulp-sourcemaps'),
    filter        = require('gulp-filter'),
    browserSync   = require('browser-sync'),
    reload        = browserSync.reload;

// Static server
gulp.task('browser-sync', function(cb) {
    browserSync({
        server: {
            baseDir: './'
        },
        port: 3001,
        browser: ['google chrome', 'firefox']
      },
      function (err) {
        if (!err) {
          console.log('BrowserSync is ready!');
        }
        else {
          console.error('Error', err.message);
        }
        cb();
      }
    );
});

gulp.task('clean', function(cb){
  del([
    'css/*.*',
    'css/maps/*.*',
    'css/maps'
    ],
    function (err) {
      if (!err) {
        console.log('Files deleted');
      }
      else {
        console.error('Error', err.message);
      }
      cb();
    });
});

// Susy requires SASS 3.3 => libSass is out
gulp.task('libsass', function () {
  return gulp.src('scss/screen.scss')
    .pipe(libSass({
      sourcemap: true,
      require: ['susy']
    }))
    .on('error', function (err) {
        console.error('Error', err.message);
    })
    .pipe(gulp.dest('css'));
});

// Inline sourcemaps (requires gulp-ruby-sass@1.0.0-alpha)
gulp.task('sass', function() {
  // Note: Use gulp-ruby-sass instead of gulp.src to compile a file or directory.
  // Note: gulp-ruby-sass only supports single directory...
  // ....it is supposed to be either a directory, a group of directories,
  // or a glob of indv. files.
  return rubySass('scss', {
      style: 'expanded',
      compass: true,
      require: ['susy'],
      sourcemap: true
    })
    .on('error', function (err) {
      console.error('Error', err.message);
    })
    .pipe(sourcemaps.write())
    .pipe(gulp.dest('css'))
    .pipe(reload({stream:true}));
});

// External sourcemaps (hosted in css/maps/) (requires gulp-ruby-sass@1.0.0-alpha)
gulp.task('sass-ext-maps', function() {
  // Note: Use gulp-ruby-sass instead of gulp.src to compile a file or directory.
  // Note: gulp-ruby-sass only supports single directory...
  // ....it is supposed to be either a directory, a group of directories,
  // or a glob of indv. files.
  return rubySass('scss', {
      container: 'gulp-ruby-sass-simple-layout', // temporary directory used to process files
      style: 'expanded',
      compass: true,
      require: ['susy'],
      sourcemap: true
    })
    .on('error', function (err) {
      console.error('Error', err.message);
    })
    .pipe(sourcemaps.write('maps', {
        // For external map files you have to
        // host the source files...
        includeContent: false,
        // ....and set the correct sourceRoot
        sourceRoot: '/scss'
    }))
    .pipe(gulp.dest('css'))
    .pipe(filter('**/*.css')) // Filtering stream to only css files (remove map files)
    .pipe(browserSync.reload({stream:true}));
});

// This works on latest 0.7.x branch, but does not support inline source maps
// only external source map files are supported
gulp.task('sass-0.7', function () {
  return gulp.src([
    'scss/*.scss'
    ])
    .pipe(rubySass({
      style: 'expanded',
      compass: true,
      require: ['susy'],
      'sourcemap=file': true // hack: --sourcemap option does not work with gulp-ruby-sass
    }))
    .on('error', function (err) { console.log(err.message); })
    .pipe(gulp.dest('css'));

      // TODO: Minify
      //.pipe(rename({suffix: '.min'}))
      //.pipe(minifycss())
      //.pipe(gulp.dest('css'));

      //.pipe(gulp.dest('.tmp'))
      //.pipe(size());
    });

// use watch task to compile sassy files and launch BrowserSync when sassy files changes
gulp.task('watch', ['sass', 'browser-sync'], function() {
  gulp.watch('scss/*.scss', ['sass']);
});
